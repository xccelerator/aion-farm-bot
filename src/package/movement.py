import pyautogui
import time
from utilities.logger import Logger

logger = Logger()

packageName = "Movement"

class Movement:
    def rotateCamera(self):
        pyautogui.drag(63, 0, 2, button='right')

        logger.Info(packageName, "BOT rotated camera!")
    
    def moveForward(self):
        time.sleep(0.25)
        pyautogui.keyDown('w')
        pyautogui.keyDown('space')
        time.sleep(2)
        pyautogui.keyUp('space')
        pyautogui.keyUp('w')

        logger.Info(packageName, "BOT moved forward!")

    def moveRight(self):
        pyautogui.mouseDown(button='right')
        pyautogui.keyDown('d')
        pyautogui.keyDown('space')
        time.sleep(1)
        pyautogui.keyUp('space')
        pyautogui.keyUp('d')
        pyautogui.mouseUp(button='right')
        time.sleep(0.25)

        logger.Info(packageName, "BOT moved to the right!")

    def moveLeft(self):
        pyautogui.mouseDown(button='right')
        pyautogui.keyDown('a')
        pyautogui.keyDown('space')
        time.sleep(1)
        pyautogui.keyUp('space')
        pyautogui.keyUp('a')
        pyautogui.mouseUp(button='right')
        time.sleep(0.25)

        logger.Info(packageName, "BOT moved to the left!")