import pyautogui
import pytesseract
import time
from package.movement import Movement
from enum import  Enum
from utilities.logger import Logger

pytesseract.pytesseract.tesseract_cmd=r"C:\\Program Files\\Tesseract-OCR\\tesseract.exe"

logger = Logger()

packageName = "Position"

class LookDirection(Enum):
    NORTH = 0
    SOUTH = 1

class Coordinates:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
class Position:
    lookDirection = LookDirection.SOUTH

    def takeCoord(
            self,
            currentLocCoord: Coordinates
        ):
        logger.Info(packageName, "BOT trying to take coords...")

        img = pyautogui.screenshot("test.png", region=(currentLocCoord.x,currentLocCoord.y, 115,25))

        x, y = pytesseract.image_to_string(img, config='--psm 6').split(" ")
        x_int, y_int = float(x), float(y)
        return x_int, y_int

    def checkPosition(
            self,
            myPositionCoord: Coordinates, 
            characterPositionCoord: Coordinates,
            currentLocCoord: Coordinates
        ):
        logger.Info(packageName, "BOT checking current possition...")

        # press M
        pyautogui.press('m')

        # click "my position"
        time.sleep(0.25)
        # pyautogui.click(1520, 280)
        pyautogui.moveTo(myPositionCoord.x, myPositionCoord.y)
        pyautogui.mouseDown(button='left')
        time.sleep(0.25)
        pyautogui.mouseUp(button='left')

        # move to my position
        time.sleep(0.5)
        pyautogui.moveTo(characterPositionCoord.x, characterPositionCoord.y)

        # ctrl+lcm on me on map
        time.sleep(0.25)
        with pyautogui.hold('ctrl'):
            pyautogui.click(button='right')

        # press HOME button
        time.sleep(0.25)
        pyautogui.press('home')

        # press /w + space
        time.sleep(0.25)
        pyautogui.write('/w ', interval=0.25)

        # press enter
        time.sleep(0.25)
        pyautogui.press('enter')

        # press M
        time.sleep(0.25)
        pyautogui.press('m')
        
        time.sleep(0.25)
        return self.takeCoord(currentLocCoord)
    
    def fixPosition(
            self, 
            firstCoord: Coordinates,
            secondCoord: Coordinates,
            myPositionCoord: Coordinates, 
            characterPositionCoord: Coordinates,
            currentLocCoord: Coordinates
        ):
        movement = Movement()

        x, y = self.checkPosition(
                        myPositionCoord, 
                        characterPositionCoord, 
                        currentLocCoord
                    )
        
        minX = min(firstCoord.x, secondCoord.x)
        maxX = max(firstCoord.x, secondCoord.x)
        minY = min(firstCoord.y, secondCoord.y)
        maxY = max(firstCoord.y, secondCoord.y)

        if x < minX:
            if self.lookDirection == LookDirection.NORTH:
                movement.rotateCamera()
                self.lookDirection = LookDirection.SOUTH
            while x < minX:
                movement.moveForward()
                x, y = self.checkPosition(
                                myPositionCoord,
                                characterPositionCoord,
                                currentLocCoord
                            )

        if x > maxX:
            if self.lookDirection == LookDirection.SOUTH:
                movement.rotateCamera()
                self.lookDirection = LookDirection.NORTH
            while x > maxX:
                movement.moveForward()
                x, y = self.checkPosition(
                                myPositionCoord,
                                characterPositionCoord,
                                currentLocCoord
                            )

        if y > maxY:
            if self.lookDirection == LookDirection.SOUTH:
                while y > maxY:
                    movement.moveRight()
                    x, y = self.checkPosition(
                                    myPositionCoord,
                                    characterPositionCoord,
                                    currentLocCoord
                                )
            
            if self.lookDirection == LookDirection.NORTH:
                while y > maxY:
                    movement.moveLeft()
                    x, y = self.checkPosition(
                                    myPositionCoord,
                                    characterPositionCoord,
                                    currentLocCoord
                                )

        if y < minY:
            if self.lookDirection == LookDirection.SOUTH:
                while y < minY:
                    movement.moveLeft()
                    x, y = self.checkPosition(
                                    myPositionCoord,
                                    characterPositionCoord,
                                    currentLocCoord
                                )
            
            if self.lookDirection == LookDirection.NORTH:
                while y < minY:
                    movement.moveRight()
                    x, y = self.checkPosition(
                                    myPositionCoord,
                                    characterPositionCoord,
                                    currentLocCoord
                                )