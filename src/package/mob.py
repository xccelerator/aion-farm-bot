import pyautogui
import numpy as np
import time
import random
from package.position import Coordinates
from utilities.logger import Logger

logger = Logger()

packageName = "Mob"

class Mob:
    def checkMob(
            self,
            enemyHpCoord: Coordinates
        ):
        logger.Info(packageName, "BOT is checking for mob..")
        

        red = [213,  97,  78]
        green = [142, 182, 106]

        img = pyautogui.screenshot(region=(enemyHpCoord.x, enemyHpCoord.y, 200, 30))

        img_to_array = np.array(img) 

        if(np.any(np.all(red == img_to_array, axis = 2))):
            return True

        if(np.any(np.all(green == img_to_array, axis = 2))):
            return True

        return False
    
    def findMob(self):
        time.sleep(0.25)
        pyautogui.press('tab')

    def attack(
            self,
            enemyHpCoord: Coordinates,
            skills: list
        ):
        logger.Info(packageName, "BOT attacked mob...")

        time.sleep(0.25)
        for skill in skills:
            time.sleep(1.75)
            pyautogui.press(skill)

            ifJump = random.randint(0,4)

            if ifJump == 2:
                pyautogui.press('space')

            if(not self.checkMob(enemyHpCoord)):
                logger.Info(packageName, "Mob was killed!")

                return
        
    def loot(self):
        logger.Info(packageName, "BOT is looting...")

        time.sleep(2)
        pyautogui.press('c')
        time.sleep(0.75)
        with pyautogui.hold('shift'):
            pyautogui.press('c')