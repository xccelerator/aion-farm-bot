from package.mob import Mob
from package.movement import Movement
from package.position import Position, Coordinates
from utilities.logger import Logger
import time
import os

time.sleep(2)

firstCoord = Coordinates(1940, 450) 
secondCoord = Coordinates(2055, 360)
currentLocCoord = Coordinates(160, 575) # coords in chat
myPositionCoord = Coordinates(1520, 280) # my position button coord on map
characterPositionCoord = Coordinates(1160,585) # coord of charcachter on the map
enemyHpCoord = Coordinates(480, 40) # coord of enemies helth

position = Position()
mob = Mob()
movement = Movement()
logger = Logger()

skills = ['4', '4', '5', '6', '2', '3', '1', '1', '1', '7']

try:
    os.system('cls')

    logger.Info("Main", "BOT started to earn kinah!")

    while True:
        position.fixPosition(
            firstCoord, 
            secondCoord,
            myPositionCoord,
            characterPositionCoord,
            currentLocCoord,
        )

        isMob = mob.checkMob(enemyHpCoord)

        if isMob:
            while isMob:
                mob.attack(enemyHpCoord, skills)
                isMob = mob.checkMob(enemyHpCoord)
        else:
            mob.findMob()
            isMob = mob.checkMob(enemyHpCoord)

            while not isMob:
                movement.moveForward()
                position.fixPosition(
                    firstCoord,
                    secondCoord,
                    myPositionCoord,
                    characterPositionCoord,
                    currentLocCoord
                )
                mob.findMob()
                isMob = mob.checkMob(enemyHpCoord)

            while isMob:
                mob.attack(enemyHpCoord, skills)
                isMob = mob.checkMob(enemyHpCoord)

        mob.loot()
except Exception as e:
    logger.Error('Main', e)