import time

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class Logger:
    def CurrentTime(self):
        return time.strftime("%H:%M:%S", time.localtime())
    
    def Info (self, serviceName, text):
        print(f"{bcolors.OKGREEN}[{self.CurrentTime()}] - {serviceName} - INFO: {text}{bcolors.ENDC}")

    def Warning (self, serviceName, text):
        print(f"{bcolors.WARNING}[{self.CurrentTime()}] - {serviceName} - WARNING: {text}{bcolors.ENDC}")

    def Error (self, serviceName, text):
        print(f"{bcolors.FAIL}[{self.CurrentTime()}] - {serviceName} - ERROR: {text}{bcolors.ENDC}")