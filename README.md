# Installation
```
git clone https://gitlab.com/xccelerator/aion-farm-bot.git
cd aion-farm-bot
Python farmBot.py
```

# Requirements:
- Python
- pyautogui
- PIL 
- pytesseract
- numpy as np
